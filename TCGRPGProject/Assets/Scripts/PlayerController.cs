﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Character {

	public string startPoint;
	public int directionFace;
	public GameInputManager inputManager;
	public Transform objectListHolder;
	public Transform exitListHolder;
	public int indexAhead;
	public List<GameObject> objectPool;
	public List<GameObject> exitList;
	public bool dialogueOpened;
	private Transform tr;
	private Vector2 direction;
	private static bool playerExists = false;

	public void Start() {
		objectListHolder = GameObject.FindGameObjectWithTag ("ObjectList").transform;
		exitListHolder = GameObject.FindGameObjectWithTag ("ExitList").transform;
		objectPool = new List<GameObject> ();
		for (int i = 0; i < objectListHolder.childCount; i++) {
			objectPool.Add (objectListHolder.GetChild (i).gameObject);
		}
		exitList = new List<GameObject> ();
		for (int i = 0; i < exitListHolder.childCount; i++) {
			exitList.Add (exitListHolder.GetChild (i).gameObject);
		}
		dialogueOpened = false;
		curPos = transform.position;
		tr = transform;
		anim = GetComponent<Animator> ();
		CharacterIndexY = -(transform.position.y + 1);
		CharacterIndex = mapObject.GetTileIndex (transform.position.x, CharacterIndexY);
		if (!playerExists) {
			playerExists = true;
			DontDestroyOnLoad (transform.gameObject);
		} else {
			Destroy (gameObject);
		}
	}

	void Update()
	{
		anim.SetFloat ("DirX", dirX);
		anim.SetFloat ("DirY", dirY);
		if (Input.GetKey (inputManager.BButton)) {
			speed = 6f;
		} else {
			speed = 3.5f;
		}
		if (!dialogueOpened) {
			if (Input.GetKey (inputManager.rightKey) && !Input.GetKey (inputManager.leftKey) && !Input.GetKey (inputManager.upKey) && !Input.GetKey (inputManager.downKey) && !isMoving) {
				MovementInput (1);
			}
			if (Input.GetKey (inputManager.leftKey) && !Input.GetKey (inputManager.rightKey) && !Input.GetKey (inputManager.upKey) && !Input.GetKey (inputManager.downKey) && !isMoving) {
				MovementInput (2);
			}
			if (Input.GetKey (inputManager.upKey) && !Input.GetKey (inputManager.rightKey) && !Input.GetKey (inputManager.leftKey) && !Input.GetKey (inputManager.downKey) && !isMoving) {
				MovementInput (3);
			}
			if (Input.GetKey (inputManager.downKey) && !Input.GetKey (inputManager.rightKey) && !Input.GetKey (inputManager.leftKey) && !Input.GetKey (inputManager.upKey) && !isMoving) {
				MovementInput (4);
			}
			if (Input.GetKeyDown (inputManager.StartButton) && !isMoving) {
				print ("You pressed the Start button.");
			}	
			if (Input.GetKeyDown (inputManager.SelectButton) && !isMoving) {
				print ("You pressed the Select button. (No one ever presses the select button...)");
			}
		}
		if (Input.GetKeyDown(inputManager.AButton) && !isMoving){
			if (dialogueManager.alreadyInteracting == false) {
				CheckForDialogue ();
			}
			else if (dialogueManager.alreadyInteracting == true && dialogueManager.readyToDisplay == true) {
				dialogueManager.DisplayNextSentence ();
			}
		}
		CharacterUpdate ();
}
	//Controls movement based on directional input
	public void MovementInput (int movementDirection){
		if (movementDirection == 1) {
			anim.SetBool ("Movement", true);
			nextIndex = mapObject.GetTileIndex ((transform.position.x + 1), -transform.position.y);
			DirectionFace (1);
			direction = Vector2.right;
			directionFace = 1;
			if (mapObject.IsTileWall (transform.position.x + 1, -transform.position.y) == false) {
				curPos.x = transform.position.x + 1;
				isMoving = true;
				mapObject.ChangeTileIndex (nextIndex);
			}
		}
		if (movementDirection == 2) {
			anim.SetBool ("Movement", true);
			DirectionFace (2);
			nextIndex = mapObject.GetTileIndex ((transform.position.x - 1), -transform.position.y);
			direction = Vector2.left;
			directionFace = 2;
			if (mapObject.IsTileWall (transform.position.x - 1, -transform.position.y) == false) {
				curPos.x = transform.position.x - 1;
				isMoving = true;
				mapObject.ChangeTileIndex (nextIndex);
			}
		}
		if (movementDirection == 3) {
			anim.SetBool ("Movement", true);
			DirectionFace (3);
			nextIndex = mapObject.GetTileIndex ((transform.position.x), -(transform.position.y + 1));
			direction = Vector2.up;
			directionFace = 3;
			if (mapObject.IsTileWall (transform.position.x, -(transform.position.y + 1)) == false) {
				curPos.y = transform.position.y + 1;
				isMoving = true;
				mapObject.ChangeTileIndex (nextIndex);
			}
		}
		if (movementDirection == 4) {
			anim.SetBool ("Movement", true);
			DirectionFace (4);
			nextIndex = mapObject.GetTileIndex ((transform.position.x), -(transform.position.y - 1));
			direction = Vector2.down;
			directionFace = 4;
			if (mapObject.IsTileWall (transform.position.x, -(transform.position.y - 1)) == false) {
				curPos.y = transform.position.y - 1;
				isMoving = true;
				mapObject.ChangeTileIndex (nextIndex);
			}
		}
	}
	public void CheckForDialogue(){
		if (directionFace == 1) {
			indexAhead = CharacterIndex + 1;
		}
		if (directionFace == 2) {
			indexAhead = CharacterIndex - 1;
		}
		if (directionFace == 3){
			indexAhead = CharacterIndex - (mapObject.rObject.layers.width);
		}
		if (directionFace == 4){
			indexAhead = CharacterIndex + (mapObject.rObject.layers.width);
		}
		print (indexAhead);
		for (int i = 0; i < objectPool.Count; i++){
			if (objectPool[i].GetComponent<Entity>().CharacterIndex == indexAhead && objectPool[i].GetComponent<Entity>().isMoving == false){
				objectPool[i].GetComponent<DialogueTrigger>().TriggerDialogue();
				dialogueOpened = true;
				break;
			}
		}
	}
	public void EnterNewRoom(Vector3 newPos) {
		print ("Grabbing new items...");
		mapObject = FindObjectOfType<ReadJson> ();
		transform.position = newPos;
		curPos = transform.position;
		objectListHolder = GameObject.FindGameObjectWithTag ("ObjectList").transform;
		exitListHolder = GameObject.FindGameObjectWithTag ("ExitList").transform;
		objectPool = new List<GameObject> ();
		for (int i = 0; i < objectListHolder.childCount; i++) {
			objectPool.Add (objectListHolder.GetChild (i).gameObject);
		}
		exitList = new List<GameObject> ();
		for (int i = 0; i < exitListHolder.childCount; i++) {
			exitList.Add (exitListHolder.GetChild (i).gameObject);
		}
		CharacterIndexY = -(transform.position.y + 1);
		CharacterIndex = mapObject.GetTileIndex (transform.position.x, CharacterIndexY);
		tr = transform;
	}
}