﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public GameObject followTarget;
	private Vector3 targetPosition;
	public float moveSpeed;
	public BoxCollider2D boundBox;
	private Vector3 minBounds;
	private Vector3 maxBounds;
	private Camera cameraObject;
	private float halfHeight;
	private float halfWidth;
	private static bool cameraExists = false;

	// Use this for initialization
	void Start () {
		minBounds = boundBox.bounds.min;
		maxBounds = boundBox.bounds.max;
		cameraObject = GetComponent<Camera> ();
		halfHeight = cameraObject.orthographicSize;
		halfWidth = halfHeight * Screen.width / Screen.height;
		if (!cameraExists) {
			cameraExists = true;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}

	}
	
	// Update is called once per frame
	void LateUpdate () {
		targetPosition = new Vector3 (followTarget.transform.position.x, followTarget.transform.position.y, transform.position.z);
		transform.position = targetPosition;

		float clampX = Mathf.Clamp (transform.position.x, minBounds.x + halfWidth, maxBounds.x - halfWidth);
		float clampY = Mathf.Clamp (transform.position.y, minBounds.y + halfHeight, maxBounds.y - halfHeight);
		transform.position = new Vector3 (clampX, clampY, transform.position.z);
	}

	public void SetBounds(BoxCollider2D newBounds){
		boundBox = newBounds;
		minBounds = boundBox.bounds.min;
		maxBounds = boundBox.bounds.max;
	}
}
