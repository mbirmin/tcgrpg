﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Net;
using UnityEngine.UI;

public class ReadJson : MonoBehaviour{

	public TextAsset mapJSONFile;
	public RootObject rObject;

	[System.Serializable]
	public class MapData
	{
		public int[] data;
		public int height;
		public int width;
	}

	[System.Serializable]
	public class RootObject
	{
		public MapData layers;
	}

	public int GetTileIndex (float xPos, float yPos){
		int xIntPos = (int)xPos;
		int yIntPos = (int)yPos;
		int index = xIntPos + (yIntPos * rObject.layers.width);
		return index;
	}

	public int ChangeTileIndex (int index){
		return rObject.layers.data [index] = 1;
	}

	public int FreeTileIndex (int index){
		return rObject.layers.data [index] = 0;
	}

	public bool IsTileWall (float xPos, float yPos){
		int index = GetTileIndex (xPos, yPos);
		if (rObject.layers.data [index] == 0) {
			return false;
		} else {
			return true;
		}
	}

	void Awake () {
		rObject = JsonUtility.FromJson<RootObject> (mapJSONFile.ToString());
	}
}