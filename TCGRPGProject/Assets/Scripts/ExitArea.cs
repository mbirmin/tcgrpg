﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitArea : MonoBehaviour {
	
	public PlayerController player;
	public Vector3 entrancePosition;
	public int exitIndex;
	public ReadJson connectedRoomInfo;
	public ReadJson mapObject;
	public string currentRoom;
	public string nextRoom;
	private float exitIndexY;

	// Use this for initialization
	void Awake () {
		player = FindObjectOfType<PlayerController> ();
		mapObject = FindObjectOfType<ReadJson> ();
		SetIndex ();
	}

	void Start(){
		SceneManager.sceneLoaded += MovePlayer;
	}
	
	//Update is called once per frame
	void Update () {
		if (player.CharacterIndex == exitIndex) {
			SceneManager.LoadScene(nextRoom);
		}
	}

	void SetIndex () {
		exitIndexY = -(transform.position.y);
		exitIndex = mapObject.GetTileIndex (transform.position.x, exitIndexY);
	}

	void MovePlayer(Scene scene, LoadSceneMode mode) {
		if (player.CharacterIndex == exitIndex) {
			print ("Player will move to new room");
			player.mapObject = connectedRoomInfo;
			player.EnterNewRoom(entrancePosition);
		}
	}
}
