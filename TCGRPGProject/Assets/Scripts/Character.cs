﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : Entity {

	[Header("Animator")]
	public Animator anim;
	[Header("Current Position")]
	public Vector3 curPos;
	[Header("Character Speed")]
	public float speed;
	protected int nextIndex;
	protected float dirX;
	protected float dirY;
	protected float lastX;
	protected float lastY;


	// Use this for initialization
	void Start () {
		anim.SetFloat ("DirX", dirX);
		anim.SetFloat ("DirY", dirY);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	protected void MovingUpdate () {
		if (isMoving) {
			transform.position = Vector3.MoveTowards (transform.position, curPos, Time.deltaTime * speed);
			if (transform.position == curPos) {
				mapObject.FreeTileIndex (oldIndex);
				isMoving = false;
				CharacterIndexY = -(transform.position.y);
				CharacterIndex = mapObject.GetTileIndex (transform.position.x, CharacterIndexY);
				SetSpace ();
			}
		} else {
			anim.SetFloat ("LastDirX", lastX);
			anim.SetFloat ("LastDirY", lastY);
			anim.SetBool ("Movement", false);
		}
	}
	protected void CharacterUpdate (){
		MovingUpdate ();
	}
	protected void DirectionFace(int directions) {
		if (directions == 1) {
			dirX = 1f;
			dirY = 0f;
			lastX = 1f;
			lastY = 0f;
		}
		if (directions == 2) {
			dirX = -1f;
			dirY = 0f;
			lastX = -1f;
			lastY = 0f;
		}
		if (directions == 3) {
			dirX = 0f;
			dirY = 1f;
			lastX = 0f;
			lastY = 1f;

		}
		if (directions == 4) {
			dirX = 0f;
			dirY = -1f;
			lastX = 0f;
			lastY = -1f;
		}
	}
	public override IEnumerator FinishDialogue(){
		yield return new WaitForSeconds(Random.Range(1.5f, 4f));
	}
}

