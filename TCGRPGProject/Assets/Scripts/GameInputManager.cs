﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInputManager : MonoBehaviour {
	public static GameInputManager inputManager;

	public KeyCode upKey;
	public KeyCode downKey;
	public KeyCode rightKey;
	public KeyCode leftKey;
	public KeyCode AButton;
	public KeyCode BButton;
	public KeyCode StartButton;
	public KeyCode SelectButton;

	void Awake(){
		if (inputManager == null) {
			DontDestroyOnLoad (gameObject);
			inputManager = this;
		} else if (inputManager != this) {
			Destroy (gameObject);
		}

		upKey = KeyCode.UpArrow;
		downKey = KeyCode.DownArrow;
		rightKey = KeyCode.RightArrow;
		leftKey = KeyCode.LeftArrow;
		AButton = KeyCode.Z;
		BButton = KeyCode.X;
		StartButton = KeyCode.Return;
		SelectButton = KeyCode.RightControl;
	}
}
