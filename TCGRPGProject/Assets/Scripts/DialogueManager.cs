﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

	private Queue<string> sentences;
	public PlayerController playerObject;
	List<GameObject> objectPool;
	public int objectSpeakingIndex;
	public bool alreadyInteracting = false;
	public GameObject dialogueBox;
	public GameObject nameBox;
	public Text nameText;
	public Text dialogueText;
	public bool readyToDisplay;

	// Use this for initialization
	void Start () {
		playerObject = FindObjectOfType<PlayerController> ();
		objectPool = playerObject.objectPool;
		readyToDisplay = true;
		sentences = new Queue<string> ();
		dialogueBox.SetActive (false);
	}
	public void StartDialogue (Dialogue dialogue){
		nameText.text = dialogue.NPCName;
		objectSpeakingIndex = playerObject.indexAhead;
		sentences.Clear ();
		dialogueBox.SetActive (true);
		if (dialogue.NPCName == " ") {
			nameBox.SetActive (false);
		} else {
			nameBox.SetActive (true);
		}
		foreach (string sentence in dialogue.sentences)
		{
			sentences.Enqueue (sentence);
		}
		DisplayNextSentence ();
	}

	public void DisplayNextSentence() {
		if (sentences.Count == 0) {
			EndDialogue ();
			return;
		}
		alreadyInteracting = true;
		string sentence = sentences.Dequeue();
		StopAllCoroutines ();
		StartCoroutine (TypeSentence (sentence));
	}

	IEnumerator TypeSentence (string sentence){
		dialogueText.text = "";
		foreach (char letter in sentence.ToCharArray()) {
			dialogueText.text += letter;
			readyToDisplay = false;
			yield return null;
		}
		readyToDisplay = true;
	}

	public void EndDialogue(){
		dialogueBox.SetActive (false);
		alreadyInteracting = false;
		readyToDisplay = true;
		playerObject.dialogueOpened = false;
		for (int i = 0; i < objectPool.Count; i++) {
			if (objectPool [i].GetComponent<Entity>().CharacterIndex == objectSpeakingIndex) {
				StartCoroutine(objectPool [i].GetComponent <Entity>().FinishDialogue ());
			}
		}
	}
}