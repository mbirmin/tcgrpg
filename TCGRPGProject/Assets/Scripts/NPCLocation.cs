﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCLocation : Character {

	[Header("NPC Booleans")]
	public bool needsRotation;
	public bool looksAround;
	public bool certainDirection;
	public bool canWalk;
	[Header("Walking Area")]
	public int[] availableWalkSpaces;
	public int directions;
	private float waitTime;
	private int walk;

	// Use this for initialization
	void Start () {
		curPos = transform.position;
		if (needsRotation) {
			anim = GetComponent<Animator> ();
		}
		anim.SetBool ("doneTalking", true);
		SetSpace ();
		waitTime = Random.Range (0.5f, 2.5f);
		if (needsRotation && looksAround) {
			StartCoroutine ("WalkOrTurn");
		} else {
			DirectionFace (directions);
		}
	}

	// Update is called once per frame
	void Update () {
		if (needsRotation) {
			if (dialogueManager.alreadyInteracting && playerObject.indexAhead == CharacterIndex && !isMoving) {
				TurnNPC ();
				StopCoroutine ("WalkOrTurn");
			}
		}
		if (needsRotation) {
			anim.SetFloat ("DirX", dirX);
			anim.SetFloat ("DirY", dirY);
			anim.SetFloat ("LastDirX", lastX);
			anim.SetFloat ("LastDirY", lastY);
		}
		CharacterUpdate ();
	}

	IEnumerator WalkOrTurn(){
		while (true) {
			if (!canWalk) {
				walk = 0;
			} else {
				walk = Random.Range (0, 2);
			}
			directions = Random.Range (1, 5);
			waitTime = Random.Range (1.5f, 3f);
			if (walk == 0) {
				DirectionFace (directions);
			} else if (walk == 1) {
				WalkNPC (directions);
			}
			yield return new WaitForSeconds (waitTime);
		}
	}

	void WalkNPC (int directions){
		if (directions == 1) {
			DirectionFace (1);
			nextIndex = mapObject.GetTileIndex ((transform.position.x + 1), -transform.position.y);
			for (int i = 0; i < availableWalkSpaces.Length; i++) {
				if (availableWalkSpaces [i] == nextIndex && mapObject.IsTileWall ((transform.position.x + 1), -transform.position.y) == false) {
					oldIndex = CharacterIndex;
					anim.SetBool ("Movement", true);
					curPos.x = transform.position.x + 1;
					isMoving = true;
					mapObject.ChangeTileIndex (nextIndex);
				}
			}
		}
		if (directions == 2) {
			DirectionFace (2);
			nextIndex = mapObject.GetTileIndex ((transform.position.x - 1), -transform.position.y);
			for (int i = 0; i < availableWalkSpaces.Length; i++) {
				if (availableWalkSpaces [i] == nextIndex && mapObject.IsTileWall ((transform.position.x - 1), -transform.position.y) == false) {
					oldIndex = CharacterIndex;
					anim.SetBool ("Movement", true);
					curPos.x = transform.position.x - 1;
					isMoving = true;
					mapObject.ChangeTileIndex (nextIndex);
				}	
			}
		}
		if (directions == 3) {
			DirectionFace (3);
			nextIndex = mapObject.GetTileIndex ((transform.position.x), -(transform.position.y + 1));
			for (int i = 0; i < availableWalkSpaces.Length; i++) {
				if (availableWalkSpaces [i] == nextIndex && mapObject.IsTileWall((transform.position.x), -(transform.position.y + 1)) == false) {
					oldIndex = CharacterIndex;
					anim.SetBool ("Movement", true);
					mapObject.ChangeTileIndex(nextIndex);
					curPos.y = transform.position.y + 1;
					isMoving = true;
				}
			}
		}
		if (directions == 4) {
			DirectionFace (4);
			nextIndex = mapObject.GetTileIndex ((transform.position.x), -(transform.position.y - 1));
			for (int i = 0; i < availableWalkSpaces.Length; i++) {
					if (availableWalkSpaces [i] == nextIndex && mapObject.IsTileWall ((transform.position.x), -(transform.position.y - 1)) == false) {
						oldIndex = CharacterIndex;
						anim.SetBool ("Movement", true);
						curPos.y = transform.position.y - 1;
						isMoving = true;
						mapObject.ChangeTileIndex (nextIndex);
					}
				}
			}
		}

		void TurnNPC (){
			if (playerObject.directionFace == 1) {
			DirectionFace (2);
			}
			if (playerObject.directionFace == 2) {
			DirectionFace (1);
			}
			if (playerObject.directionFace == 3) {
			DirectionFace (4);
			}
			if (playerObject.directionFace == 4) {
			DirectionFace (3);
			}
			anim.SetBool ("doneTalking", false);
		}

		public override IEnumerator FinishDialogue (){
			anim.SetBool ("doneTalking", true);
			yield return new WaitForSeconds(Random.Range(1f, 3f));
			if (looksAround) {
				StartCoroutine ("WalkOrTurn");
			}
			if (certainDirection) {
				DirectionFace (directions);
		}
	}
}
