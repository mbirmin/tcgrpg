﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStartPoint : MonoBehaviour {

	private PlayerController player;
	private CameraController camera;
	public BoxCollider2D boundsBox;

	public string pointName;


	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController> ();
		if (player.startPoint == pointName) {
			player.speed = 0f;
			player.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, player.transform.position.z);
			player.curPos = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, player.transform.position.z);
			player.isMoving = false;
			player.anim.SetBool ("WalkRight", false);
			player.anim.SetBool ("WalkLeft", false);
			player.anim.SetBool ("WalkUp", false);
			player.anim.SetBool ("WalkDown", false);

			camera = FindObjectOfType<CameraController> ();
			camera.boundBox = boundsBox;
		}
		//boundBox = 
		//camera.boundBox = boundBox;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
