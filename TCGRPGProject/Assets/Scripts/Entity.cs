﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour {

	[Header("Static Reference Objects")]
	public PlayerController playerObject;
	public DialogueManager dialogueManager;
	[Header("Map Object")]
	public ReadJson mapObject;
	[Header("Space Occupied")]
	public int CharacterIndex;
	public bool isMoving;
	protected float CharacterIndexY;
	protected int oldIndex;

	// Use this for initialization
	void Awake () {
		playerObject = FindObjectOfType<PlayerController> ();
		dialogueManager = FindObjectOfType<DialogueManager> ();
		mapObject = FindObjectOfType<ReadJson> ();
	}

	void Start(){
		SetSpace ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	protected void SetSpace () {
		CharacterIndexY = -(transform.position.y);
		CharacterIndex = mapObject.GetTileIndex (transform.position.x, CharacterIndexY);
		oldIndex = CharacterIndex;
		mapObject.ChangeTileIndex (CharacterIndex);
	}
	public virtual IEnumerator FinishDialogue (){
		yield return new WaitForSeconds(Random.Range(1.5f, 4f));
	}
}
